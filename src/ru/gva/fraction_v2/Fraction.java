package ru.gva.fraction_v2;


/**
 * @author Gavrikov Vadim 15OIT18.
 */
public class Fraction {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Fraction(int num) {
        this(num, 1);
    }

    public Fraction() {
        this(1, 1);
    }


    @Override
    public String toString() {
        return num + "/" + denum;
    }


    public Fraction summa(Fraction fraction1) {
        Fraction fraction2 = new Fraction();
        if (this.denum == fraction1.denum) {
            fraction2.denum = this.denum;
            fraction2.num = this.num + fraction1.num;
        } else {
            fraction2.denum = this.denum * fraction1.denum;
            fraction2.num = this.num * fraction1.denum + fraction1.num * this.denum;
        }
        return fraction2.reduction();
    }

    public Fraction difference(Fraction fraction1) {
        Fraction fraction2 = new Fraction();
        if (this.denum == fraction1.denum) {
            fraction2.denum = this.denum;
            fraction2.num = this.num - fraction1.num;
        } else {
            fraction2.denum = this.denum * fraction1.denum;
            fraction2.num = this.num * fraction1.denum - fraction1.num * this.denum;
        }
        return fraction2.reduction();
    }

    public Fraction div(Fraction fraction1) {
        Fraction fraction = new Fraction(this.num * fraction1.denum, this.denum * fraction1.num);
        return fraction.reduction();
    }

    public Fraction mult(Fraction fraction) {
        Fraction fraction1 = new Fraction(this.num * fraction.num, this.denum * fraction.denum);
        return fraction1.reduction();

    }

    /**
     * Метод сокращяет дроб.
     *
     * @return в зависимости от возможности сокращение если дробь можно скратить то сокращяет и возвращяет, если нет то исходный вариант.
     */
    public Fraction reduction() {
        if (this.num == 0){
            return null;
        }
        if (this.num < 0 && this.denum < 0) {
            this.denum = -this.denum;
            this.num = -this.num;
        }
        Fraction fraction1 = new Fraction(this.num, this.denum);
        int a = fraction1.gcd();
        if (a > 1) {

            return new Fraction(this.num / a, this.denum / a);

        }

        return new Fraction(this.num, this.denum);

    }

    public int gcd() {
        int a = this.num;
        int b = this.denum;
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }


}



