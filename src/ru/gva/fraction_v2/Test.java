package ru.gva.fraction_v2;

import java.util.regex.*;
import java.io.*;


/**
 * Тестовый класс содержащий методы для вычислений с дробями.
 *
 * @author Gavrikov Vadim 15OIT18.
 */
public class Test {

    public static void main(String[] args) throws IOException {

        File isCheck = new File("/home/vadim/Документы/project1/Fractional_v.2/src/ru/gva/fraction_v2/test.txt");
        if (!isCheck.exists()) {
            System.err.println("Файл не существует или ошибка при указании пути к нему.");
            return;
        }
        BufferedReader input = new BufferedReader(new FileReader(isCheck));
        BufferedWriter output = new BufferedWriter(new FileWriter("/home/vadim/Документы/project1/Fractional_v.2/src/ru/gva/fraction_v2/output.txt", false));
        BufferedWriter notAValidString = new BufferedWriter(new FileWriter("/home/vadim/Документы/project1/Fractional_v.2/src/ru/gva/fraction_v2/error.txt", false));
        String string;
            for (int i = 1;(string = input.readLine()) != null; i++) {
                if (!isCheck(string)) {
                    notAValidString.write(String.valueOf(i)+") " + string);
                    notAValidString.append("\n");
                    continue;
                }
                String[] strings = splitString(string);
                Fraction firstFraction = saveFraction(strings[0]);
                Fraction secondFraction = saveFraction(strings[2]);
                output.write(String.valueOf(i)+") "+ String.valueOf(math(firstFraction, secondFraction, strings[1])) + ";");
                output.append("\n");
            }
            output.close();
            notAValidString.close();
            input.close();



    }


    /**
     * Метод преобразует элементы строчного массива и сохраняет их в обьект типа Fraction.
     *
     * @param string Элемент массива.
     * @return дробь
     */
    private static Fraction saveFraction(String string) {
        String[] strings = splitarray(string);
        return new Fraction(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
    }

    /**
     * Метод разделяет строку на части по пробелам.
     *
     * @param str строка
     * @return разделеная строка по пробелам.
     */
    private static String[] splitString(String str) {
        return str.split(" ");
    }


    /**
     * Метод проверяет строку на корректность ввода.
     *
     * @param string строка
     * @return результат проверки
     */
    private static boolean isCheck(String string) {
        Pattern pattern = Pattern.compile("^-?[1-9][0-9]*/-?[1-9][0-9]*\\s[*:+-]\\s-?[1-9][0-9]*/-?[1-9][0-9]*$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }


    /**
     * Метод, который разделяет строку на числитель и знаменатель.
     *
     * @param string строка
     * @return разделеную строку.
     */
    private static String[] splitarray(String string) {
        return string.split("/");
    }

    /**
     * Метод вызывает другие методы в зависимости от действия хранящегося в строке string,
     * для выполнения арифметических действий над 1 и 2 обьектом.
     *
     * @param fraction  первая дробь
     * @param fraction1 вторая дробь
     * @param string    действие
     * @return результат вычисления 1 и 2 дроби.
     */
    private static Fraction math(Fraction fraction, Fraction fraction1, String string) {
        switch (string) {
            case "+":
                return fraction.summa(fraction1);
            case "-":
                return fraction.difference(fraction1);
            case ":":
                return fraction.div(fraction1);
            case "*":
                return fraction.mult(fraction1);
        }
        return null;
    }


}

